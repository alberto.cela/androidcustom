# Plantilla de repositorio de personalización

Este repositorio permite establecer una configuración para generar una versión personalizada de la aplicación [ucclient-android](https://gitlab.com/quobis/DEV/ucclient-android)

## Cómo generar la aplicación
1. Navegar a la tarea en Jenkins encargada de la personalización. [Enlace](http://ci.internal.quobis.com:8080/blue/organizations/jenkins/ucclient-android/branches/)
2. Sobre la rama disponible, en el lado derecho. Aparecerá un icono con forma de reproducir, con un texto descriptivo que indicar "iniciar". Haciendo sobre este icono comenzará la tarea de personalización.
3. Aparecerá un mensaje emergente, indicando que la tarea se ha iniciado con un indicativo para abrir. Pulsaremos este mensaje o simplemente entraremos en la tarea haciendo click en el nombre de la rama.
4. A partir de este momento, si existe una máquina disponible, se deberá seguir la interfaz para indicar los parámetros deseados
    1. URL del repositorio
    2. Rama del repositorio
5. Una vez finalizado el proceso, en la parte superior, se podrá acceder a una pestaña llamada "Artefactos". Si se hace click en esta pestaña, se tendrá acceso a la descarga de la aplicación. 
   

## Estructura del repositorio
 
En primer lugar se repasará el contenido organizado en carpetas del repositorio:

```
collaboratorAndroidTemplate
├── /Colors
├── /Fonts
├── /Images
│   └── /AppIcon
├── /Sounds
├── /Strings
├── collaborator.properties
├── google-services.json
└── README.md

```

Para la personalización de la aplicación será necesario mantener esta jerarquía de carpetas. Se ha realizado esta división para favorecer la gestión de los datos y facilitar la actualización de los diferentes campos que se pueden modificar.
Es destacable la inclusión de dos ficheros que se encuentran en el directorio raíz del repositorio:

- `collaborator.properties`: Fichero en donde se define en el entorno que utilizará la aplicación personalizada. Sigue el siguiente formato:

```
server_url=https://live.dev.quobis.es

```

- `google-services.json` : Fichero que contiene la configuración del proyecto de Firebase asociado a la nueva aplicación. Será necesaria su generación. En el siguiente enlace se definen los pasos necesarios para la creación de un nuevo proyecto así como la generación del fichero de configuración [enlace](https://quobis.atlassian.net/wiki/spaces/DevTeam/pages/1794703524/How+to+create+mobile+apps)


A continuación se describirán cada uno de los contenidos que admiten las distintas carpetas.

### Colors
 Carpeta que contiene los ficheros relacionados con la configuración de colores de la aplicación. En caso de querer modificar los colores de la aplicación será necesario incluir el siguiente fichero:
- `colors.xml`. En este fichero se definirá el código Hexadecimal de cada color que se quiera modificar. 
	
	```
    <?xml version="1.0" encoding="utf-8"?>
    
    <resources>
      <color name="mid_green">#D33737</color>, 
      <color name="dark_green">#36A936</color>, 
      <color name="accent_green">#F29323</color>
      <color name="accent_blue">#fff222</color>
    </resources>
    
    ```
 
    1. Cada una de las entradas son opcionales.
    2. El nombre de los diferentes colores no debe ser cambiado.
    3. Fichero opcional. No es necesario incluir este fichero, e caso de no incluirlo se obtendrán los colores por defecto de la aplicación.
 
### Fonts
Carpeta que contiene los ficheros relacionados con las fuentes que se quieran incluir en la aplicación. 
- `montserrat.ttf`: Fuente que definará la fuente por defecto dentro de la aplicación.
- `montserrat_bold.ttf`: Fuente que definará la fuente tipo "bold" por defecto dentro de la aplicación.

    Para la inclusión de nuevas fuentes se añadirán los ficheros de tipo siguiendo las siguientes limitaciones:
    1. Los nombres de las fuentes no pueden contener el caracter ".".
    2. Sólo podrá hacer dos archivos de tipo fuente.
    3. No es necesario establecer un nombre en concreto a los ficheros.
    4. El tipo de fuente "bold" vendrá identificado por contener "_bold" en su nombre.
    5. Los diferentes formatos son: ".ttf", ".ttc" y ".otf"
    6. Ficheros opcionales. No es necesario incluir estos ficheros, en caso de no incluirlos se obtendrán las fuentes por defecto de la aplicación.
     
### Images
Carpeta que contiene todos los ficheros necesarios para establecer las diferentes imágenes de la aplicación.

Se puede acceder a una guía sobre cómo generar cualquier fichero de imagen que se encuentre en esta carpeta y subcarpetas a través del siguiente [enlace](https://quobis.atlassian.net/wiki/spaces/DevTeam/pages/3059286053/How+to+generate+custom+APK+Android+app+from+Jenkins#Icono-de-la-aplicaci%C3%B3n).

En primera instancia sólo se gestionarán los siguientes dos ficheros de imágenes:
	
- `ic_collaborator.xml`: Imagen vectorial que establece la imagen que aparece en la barra superior de la aplicacion.
- `ic_collaborator_logo.xml`: Imagen vectorial que establece el logo que aparece en la vista del login.

1. Se debe mantener el mismo nombre.
2. Ficheros opcionales. No es necesario incluir estos ficheros, en caso de no incluirlos se obtendrán las imágenes por defecto de la aplicación.


### Images/AppIcon 
 Carpeta que contiene todos los ficheros necesarios para establecer los iconos de la aplicación. En el siguiente enlace se definen las formas de generar estos recursos [enlace](https://quobis.atlassian.net/wiki/spaces/DevTeam/pages/3059286053/How+to+generate+custom+APK+Android+app+from+Jenkins#Icono-de-la-aplicaci%C3%B3n)

```
AppIcon
├── /drawable
│   └── ic_launcher_background.xml
├── /drawable-v24
│   └── ic_launcher_foreground.xml
├── /mipmap-anydpi-v26
│   └── ic_launcher.xml
│   └── ic_launcher_round.xml
├── /mipmap-hdpi
│   └── ic_launcher.png
│   └── ic_launcher_round.png
├── /mipmap-mdpi
│   └── ic_launcher.png
│   └── ic_launcher_round.png
├── /mipmap-xhdpi
│   └── ic_launcher.png
│   └── ic_launcher_round.png
├── /mipmap-xxhdpi
│   └── ic_launcher.png
│   └── ic_launcher_round.png
├── /mipmap-xxxhdpi
│   └── ic_launcher.png
│   └── ic_launcher_round.png
└── ic_launcher-playstore.png

```

Los diferentes formatos y tamaños para cada imagen vienen definidos en el siguiente [enlace](https://quobis.atlassian.net/wiki/spaces/DevTeam/pages/3059286053/How+to+generate+custom+APK+Android+app+from+Jenkins#Icono-de-la-aplicaci%C3%B3n)
- `drawable/ic_launcher_background.xml`: Imagen vectorial que define el icono adaptativo.
- `drawable-v26/ic_launcher_foreground.xml`: Imagen vectorial que define el fondo del icono adaptativo.
- `mipmap-anydpi-v26/ic_launcher.xml`: Imagen vectorial que define el icono adaptativo cuadrado.
- `mipmap-anydpi-v26/ic_launcher_round.xml`: Imagen vectorial que define el icono adaptativo sin bordes.
- `ic_launcher-playstore.png`: Imagen de alta resolución del icono de la aplicación utilizado en diferentes sitios en Google Play.
- `mipmap-XXX/ic_launcher.png`: Iconos en formato PNG con un tamaño establecido en base a la densidad de la pantalla del dispositivo.
- `mipmap-XXX/ic_launcher_round.png`: Iconos sin bordes, en formato PNG con un tamaño establecido en base a la densidad de la pantalla del dispositivo.

1. Todos los ficheros se pueden generar a partir de una imagen vectorial. Existe una guía de cómo generar los ficheros en el enlace anterior.
2. Se debe mantener la misma jerarquía y nombres de ficheros que se ha expuesto anteriormente.
    
### Sounds
Carpeta que contiene todos los ficheros relacionados con los sonidos de la aplicación.
- `ringback.mp3`: Fichero opcional que establece el sonido que se escucha al realizar una llamada saliente.
    1. Fichero limitado a un tamaño de 5 MB.
    2. El nombre del fichero deber ser `ringback.XXX`
    3. Los diferentes formatos soportados son: "flac", "mp3", "wav" y "ogg".
    4. Fichero opcional. No es necesario incluir este fichero, en caso de no incluirlo se utilizará el ringback por defecto de la aplicación.
  

### Strings
Carpeta que contiene todos los ficheros relacionados con la personalización de las diferentes cadenas de texto de la aplicación.
- `strings.xml`: Fichero opcional que permite establecer el identificador y el nombre de la aplicación. El contenido será el siguiente:

    ```
    <?xml version="1.0" encoding="utf-8"?>
    
    <resources>
        <string name="app_name">newAppName</string>
        <string name="app_id">com.quobis.newid</string>
    </resources>
    
    ```
 1. Cada una de las entradas son opcionales.
 2. El nombre de las diferentes variables no debe ser cambiado.
 3. En el caso de establecer un nuevo id, será obligatorio la incorporación del fichero `google-services.json`.